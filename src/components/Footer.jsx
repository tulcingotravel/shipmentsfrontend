import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                <footer className="footer">
                    <span className="bg-info fixed-bottom text-white text-center">All right reserved 2021 @Freud</span>
                </footer>
            </div>
        )
    }
}

export default Footer