/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import {Component} from "react";
import AuthenticationService from '../../api/config/AuthenticationService.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// reactstrap components
import {Button,Card,CardHeader,CardBody,FormGroup,Form,Input,InputGroupAddon,InputGroupText,InputGroup,Row,Col, Alert} from "reactstrap";

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
        username: '',
        password: '',
        hasLoginFailed: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.loginClicked = this.loginClicked.bind(this);

}

handleChange(event) {
    this.setState(
        {
            [event.target.name]: event.target.value
        })
}

loginClicked() {
    
    AuthenticationService.executeJwtAuthenticationService(this.state.username, this.state.password)
    .then( (response) => {
      
                AuthenticationService.registerSuccessfulLoginForJwt(response.data);                                               
                this.props.history.push('/')
            }).catch( () => {
                this.setState({hasLoginFailed: true})
            })
}
  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-3">
                <h1>Iniciar sesión</h1>
              </div> 
              <Row className="d-flex justify-content-center">
                  {this.state.hasLoginFailed && <Alert color="warning" > Datos de acceso incorrectos </Alert>}
              </Row>               
            </CardHeader>
            <CardBody className="px-lg-5 py-lg-5">
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <FontAwesomeIcon icon="envelope"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Correo electrónico" type="email" autoComplete="new-email"
                      name="username" 
                      value={this.state.username}
                      onChange={this.handleChange}/>
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <FontAwesomeIcon icon="key"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Contraseña" type="password" autoComplete="password"
                      name="password" 
                      value={this.state.password}
                      onChange={this.handleChange}/>
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="button" onClick={this.loginClicked}>
                      Iniciar sesión
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#freud"
                onClick={e => e.preventDefault()}
              >
                <small>Olvidaste tu contraseña?</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

export default Login;
