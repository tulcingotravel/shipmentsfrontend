import React, {Component} from 'react';
import { Alert } from 'reactstrap';

class WelcomeComponent extends Component {

    render() {
        return (
            <div className="container">                
                <Alert color="success">
                    Bienvenido : {this.props.match.params.name}.
                </Alert>
            </div>
        )
    }

}

export default WelcomeComponent