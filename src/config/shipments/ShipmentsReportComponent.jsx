import { Component } from 'react';
import ShipmentDataService from '../../api/config/ShipmentDataService.js';
import Header from '../../components/Headers/Header.js';
import {Row,Card,CardHeader,CardFooter,Table,Container,Col} from 'reactstrap';
import AuthenticationService from '../../api/config/AuthenticationService.js';
import Paginations from '../../components/Pages';
import '../../assets/css/Pages.scss';

class ShipmentsReportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      allShipmentsReportDetail: [],
      currentShipmentsReportDetail: [],
      currentPage: null,
      totalShipmentsReportDetail: null
    };
    this.refreshShipmentsReportDetail = this.refreshShipmentsReportDetail.bind(
      this
    );
  }

  componentDidMount() {
    this.refreshShipmentsReportDetail();
  }

  onPageChanged = (data) => {
    const { allShipmentsReportDetail } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentShipmentsReportDetail = allShipmentsReportDetail.slice(
      offset,
      offset + pageLimit
    );
    this.setState({ currentPage, currentShipmentsReportDetail, totalPages });
  };

  refreshShipmentsReportDetail() {
    AuthenticationService.setupAxiosInterceptors();
    ShipmentDataService.findAllShipmentReportById(this.state.id).then(
      (response) => {
        this.setState({ allShipmentsReportDetail: response.data });
      }
    );
  }

  render() {
    const {
      allShipmentsReportDetail,
      currentShipmentsReportDetail,
      currentPage,
      totalPages
    } = this.state;
    const totalShipmentsReportDetail = allShipmentsReportDetail.length;
    if (totalShipmentsReportDetail === 0) return null;
    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Card className='shadow'>
            <CardHeader className='border-0'>
              <h3 className='mb-0'>Envíos</h3>
              <Row className='justify-content-between'>
                {currentPage && (
                  <Col className='col-6 d-flex justify-content-end'>
                    Página {currentPage} / {totalPages}
                  </Col>
                )}
              </Row>
            </CardHeader>
            <Table className='align-items-center table-flush' responsive>
              <thead className='thead-light'>
                <tr>
                  <th scope='col'>#</th>
                  <th scope='col'>Remitente</th>
                  <th scope='col'>Lugar remitente</th>
                  <th scope='col'>Recibe</th>
                  <th scope='col'>Lugar receptor</th>
                  <th scope='col'>Teléfono</th>
                  <th scope='col'>Cantidad</th>
                  <th scope='col'>Contenido</th>
                </tr>
              </thead>
              <tbody>
                {currentShipmentsReportDetail.map((ShipmentsReportDetail) => (
                  <tr
                    key={ShipmentsReportDetail.id}
                    id={ShipmentsReportDetail.id}
                  >
                    <td>{ShipmentsReportDetail.id}</td>
                    <td>{ShipmentsReportDetail.senderPerson}</td>
                    <td>{ShipmentsReportDetail.originLocation.name}</td>
                    <td>{ShipmentsReportDetail.receiverPerson}</td>
                    <td>{ShipmentsReportDetail.destinationLocation.name}</td>
                    <td>{ShipmentsReportDetail.receiverPersonPhone}</td>
                    <td>{ShipmentsReportDetail.quantity}</td>
                    <td>{ShipmentsReportDetail.shipmentContent}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <CardFooter className='py-4'>
              <nav aria-label='...'>
                <div className='d-flex flex-row py-4 align-items-center'>
                  <Paginations
                    totalRecords={totalShipmentsReportDetail}
                    pageLimit={10}
                    pageNeighbours={1}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </nav>
            </CardFooter>
          </Card>
        </Container>
      </>
    );
  }
}

export default ShipmentsReportComponent;
