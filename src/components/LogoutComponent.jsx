import React, {Component} from 'react';
import { Container} from 'reactstrap';

class LogoutComponent extends Component {
    render() {
        return (
            <Container>
                <h1>Cerraste sesión de forma exitosa</h1>
                <div className="container">
                    Gracias por usar la aplicación.
                </div>
            </Container>
        )
    }
}

export default LogoutComponent