/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
import { Link } from 'react-router-dom';
// reactstrap components
import {DropdownMenu,DropdownItem,UncontrolledDropdown,DropdownToggle,Form,FormGroup,InputGroupAddon,InputGroupText,Input,InputGroup,Navbar,Nav,Container, Media
} from 'reactstrap';
import AuthenticationService from '../../api/config/AuthenticationService.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//FOR DESKTOP
class AdminNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      person: {},
      filter:''
    };
    this.logout = this.logout.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  logout() {
    AuthenticationService.logout();
    this.props.history.push('/auth/login');
  }

  componentDidMount() {
    AuthenticationService.setupAxiosInterceptors();
    AuthenticationService.findPersonByUsername(
      localStorage.getItem('authenticatedUser')
    ).then((response) => this.setState({ person: response.data }));
    
  }

  handleInputChange(event){
    if(event.keyCode === 13){
      this.props.history.push({pathname:'/admin/index', search: `?filter=${event.target.value}`});
      window.location.reload();
      event.preventDefault();
    }    
  }

  render() {
    localStorage.setItem('country', this.state.person.country);
    localStorage.setItem('store', this.state.person.store);
    return (
      <>
        <Navbar className='navbar-top navbar-dark' expand='md' id='navbar-main'>
          <Container fluid>
            <Form className='navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto'>
              <FormGroup className='mb-0'>
                <InputGroup className='input-group-alternative'>
                  <InputGroupAddon addonType='prepend'>
                    <InputGroupText>
                      <FontAwesomeIcon icon='search' />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder='Buscar...' type='text' onKeyDown={this.handleInputChange}/>
                </InputGroup>
              </FormGroup>
            </Form>
            <Nav className='align-items-center d-none d-md-flex' navbar>
              <UncontrolledDropdown nav>
                <DropdownToggle className='pr-0' nav>
                  <Media className='align-items-center'>
                    <span className='avatar avatar-sm rounded-circle'>
                      <FontAwesomeIcon icon='user' className='text-primary' />
                    </span>
                    <Media className='ml-2 d-none d-lg-block'>
                      <span className='mb-0 text-sm font-weight-bold'>
                        {this.state.person.name}
                      </span>
                    </Media>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-arrow' right>
                  <DropdownItem className='noti-title' header tag='div'>
                    <h6 className='text-overflow m-0'>Bienvenido!</h6>
                  </DropdownItem>
                  <DropdownItem to='/admin/user-profile' tag={Link}>
                    <FontAwesomeIcon icon='user' />
                    <span>Mi perfil</span>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem to='/logout' onClick={this.logout}>
                    <FontAwesomeIcon icon='power-off' />
                    Salir
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default AdminNavbar;
