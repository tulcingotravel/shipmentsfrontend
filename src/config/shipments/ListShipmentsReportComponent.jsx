import { Component } from 'react';
import ShipmentDataService from '../../api/config/ShipmentDataService.js';
import Header from '../../components/Headers/Header.js';
import {
  Row,
  Card,
  CardHeader,
  CardFooter,
  Table,
  Container,
  Col
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import AuthenticationService from '../../api/config/AuthenticationService.js';
import Paginations from '../../components/Pages';
import '../../assets/css/Pages.scss';

class ListShipmentsReportComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allShipmentsReport: [],
      currentShipmentsReport: [],
      currentPage: null,
      totalShipmentsReport: null
    };
    this.refreshShipmentsReport = this.refreshShipmentsReport.bind(this);
    this.viewDetailClicked = this.viewDetailClicked.bind(this);
  }

  componentDidMount() {
    this.refreshShipmentsReport();
  }

  onPageChanged = (data) => {
    const { allShipmentsReport } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentShipmentsReport = allShipmentsReport.slice(
      offset,
      offset + pageLimit
    );
    this.setState({ currentPage, currentShipmentsReport, totalPages });
  };

  refreshShipmentsReport() {
    AuthenticationService.setupAxiosInterceptors();
    ShipmentDataService.findAllShipmentReport().then((response) => {
      this.setState({ allShipmentsReport: response.data });
    });
  }

  viewDetailClicked(id) {
    this.props.history.push(`/report/shipments/${id}`);
  }

  render() {
    const {
      allShipmentsReport,
      currentShipmentsReport,
      currentPage,
      totalPages
    } = this.state;
    const totalShipmentsReport = allShipmentsReport.length;
    if (totalShipmentsReport === 0) return null;
    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Card className='shadow'>
            <CardHeader className='border-0'>
              <h3 className='mb-0'>Reporte de Envíos</h3>
              <Row className='justify-content-between'>
                {currentPage && (
                  <Col className='col-6 d-flex justify-content-end'>
                    Página {currentPage} / {totalPages}
                  </Col>
                )}
              </Row>
            </CardHeader>
            <Table className='align-items-center table-flush' responsive>
              <thead className='thead-light'>
                <tr>
                  <th scope='col'>#</th>
                  <th scope='col'>Fecha</th>
                  <th scope='col'>Creado por</th>
                  <th scope='col'>Fecha inicio</th>
                  <th scope='col'>Fech fin</th>
                  <th scope='col'>Detalle</th>
                </tr>
              </thead>
              <tbody>
                {currentShipmentsReport.map((shipmentsReport) => (
                  <tr key={shipmentsReport.id} id={shipmentsReport.id}>
                    <td>{shipmentsReport.id}</td>
                    <td>{shipmentsReport.createdAt}</td>
                    <td>
                      {shipmentsReport.createdBy.name}{' '}
                      {shipmentsReport.createdBy.firstName}
                    </td>
                    <td>{shipmentsReport.startDate}</td>
                    <td>{shipmentsReport.endDate}</td>
                    <td>
                      <Link
                        className='btn btn-outline-primary'
                        role='button'
                        to={`/admin/report/shipments/${shipmentsReport.id}`}
                        onClick={() =>
                          this.props.history.push(
                            `/admin/report/shipments/${shipmentsReport.id}`
                          )
                        }
                      >
                        <FontAwesomeIcon icon='edit' />
                        Detalle
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <CardFooter className='py-4'>
              <nav aria-label='...'>
                <div className='d-flex flex-row py-4 align-items-center'>
                  <Paginations
                    totalRecords={totalShipmentsReport}
                    pageLimit={10}
                    pageNeighbours={1}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </nav>
            </CardFooter>
          </Card>
        </Container>
      </>
    );
  }
}

export default withRouter(ListShipmentsReportComponent);
