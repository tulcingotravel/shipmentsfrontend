import { Component } from 'react';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import PersonDataService from '../../api/config/PersonDataService';
import StoreDataService from '../../api/config/StoreDataService';
import Header from '../../components/Headers/Header.js';
import moment from 'moment';
import {
  Button,
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Card,
  CardHeader,
  CardBody,
  Container
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class PersonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      name: '',
      firstName: '',
      lastName: '',
      birthdate: moment(new Date()).format('YYYY-MM-DD'),
      phone: '',
      mail: '',
      password: '',
      active: false,
      role: 0,
      store: 0,
      storeList: []
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
    this.inputChangedHandler = this.inputChangedHandler.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  onSubmit(values) {
    let person = {
      id: this.state.id,
      name: values.name,
      firstName: values.firstName,
      lastName: values.lastName,
      birthdate: values.birthdate,
      phone: values.phone,
      mail: values.mail,
      password: values.password,
      active: values.active,
      role: values.role,
      store: values.store
    };
    if (this.state.id === -1 || person.id === undefined) {
      PersonDataService.createPerson(person).then(() =>
        this.props.history.push('/persons')
      );
    } else {
      PersonDataService.updatePerson(this.state.id, person).then(() =>
        this.props.history.push('/persons')
      );
    }
  }

  validate(values) {
    let errors = {};
    if (!values.name) {
      errors.name = 'Nombre obligatorio.';
    } else if (!values.firstName) {
      errors.name = 'Apellido Paterno obligatorio.';
    } else if (!values.mail) {
      errors.name = 'Correo electrónico obligatorio.';
    } else if (!values.password) {
      errors.name = 'Contraseña obligatorio.';
    }
    return errors;
  }

  componentDidMount() {
    //Review data initialization
    const {
      id,
      name,
      firstName,
      lastName,
      birthdate,
      phone,
      mail,
      password,
      active,
      role,
      store
    } = this.props;
    this.setState({
      id,
      name,
      firstName,
      lastName,
      birthdate,
      phone,
      mail,
      password,
      active,
      role,
      store
    });
    //End review
    if (this.state.id === -1) {
      return;
    }

    PersonDataService.findPersonById(this.state.id).then((response) =>
      this.setState({
        id: response.data.id,
        name: response.data.name,
        firstName: response.data.firstName,
        lastName: response.data.lastName,
        birthdate: response.data.birthdate,
        phone: response.data.phone,
        mail: response.data.mail,
        password: response.data.password,
        active: response.data.active,
        role: response.data.role,
        store: response.data.store
      })
    );
    StoreDataService.findAll().then((response) => {
      this.setState({ storeList: response.data });
    });
  }

  inputChangedHandler(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    let {
      id,
      name,
      firstName,
      lastName,
      birthdate,
      phone,
      mail,
      password,
      active,
      role,
      store,
      storeList
    } = this.state;

    var storeListOptions = storeList.map((store) => {
      return (
        <option value={store.id} key={store.id}>
          {store.name}
        </option>
      );
    });
    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Formik
            initialValues={{
              id,
              name,
              firstName,
              lastName,
              birthdate,
              phone,
              mail,
              password,
              active,
              role,
              store
            }}
            onSubmit={this.onSubmit}
            validate={this.validate}
            enableReinitialize={true}
          >
            {(props) => (
              <Row>
                <div className='col'>
                  <Card className='shadow'>
                    <CardHeader className='bg-white border-0'>
                      <Row className='align-items-center'>
                        <h3 className='mb-0'>Ingresa los datos</h3>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form>
                        <ErrorMessage
                          name='name'
                          component='div'
                          className='alert alert-warning'
                        />
                        <ErrorMessage
                          name='firstName'
                          component='div'
                          className='alert alert-warning'
                        />
                        {/* Main info */}
                        <h6 className='heading-small text-muted mb-4'>
                          DATOS PRINCIPALES
                        </h6>
                        <div className='pl-lg-4'>
                          <FormGroup className='row'>
                            <div className='col-md-4'>
                              <Label>Nombre</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='user' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='name'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-4'>
                              <Label>Apellido Paterno</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='user' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='firstName'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-4'>
                              <Label>Apellido Materno</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='user' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='lastName'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                          </FormGroup>
                        </div>
                        <hr className='my-4' />
                        {/* Optional info */}
                        <h6 className='heading-small text-muted mb-4'>
                          INFORMACIÓN OPCIONAL
                        </h6>
                        <div className='pl-lg-4'>
                          <FormGroup className='row'>
                            <div className='col-md-6'>
                              <Label>Fecha de Nacimiento</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='birthday-cake' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='date'
                                  name='birthdate'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-6'>
                              <Label>Teléfono</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='phone' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='phone'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                          </FormGroup>
                        </div>
                        <hr className='my-4' />
                        {/* Access information */}
                        <h6 className='heading-small text-muted mb-4'>
                          DATOS DE ACCESO
                        </h6>
                        <div className='pl-lg-4'>
                          <FormGroup className='row'>
                            <div className='col-md-5'>
                              <Label>Correo</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='envelope' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='mail'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>{' '}
                            </div>
                            <div className='col-md-5'>
                              <Label>Password</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='key' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='password'
                                  name='password'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-2'>
                              <FormGroup check>
                                <Label check>
                                  <Input
                                    type='checkbox'
                                    name='active'
                                    checked={this.state.active}
                                    onChange={this.handleInputChange}
                                  />
                                  Activo
                                </Label>
                              </FormGroup>
                            </div>
                          </FormGroup>
                          <FormGroup className='row'>
                            <div className='col-md-6'>
                              <Label>Rol</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='flag' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input
                                  className='form-control'
                                  type='select'
                                  name='role'
                                  value={this.state.role}
                                  onChange={this.inputChangedHandler}
                                >
                                  <option value='1'>Admin</option>
                                  <option value='2'>Admin consultor</option>
                                  <option value='3'>Ventas</option>
                                </Input>
                              </InputGroup>
                            </div>
                            <div className='col-md-6'>
                              <Label>Tienda</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='map-marked-alt' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input
                                  className='form-control'
                                  type='select'
                                  name='store'
                                  value={this.state.store}
                                  onChange={this.inputChangedHandler}
                                >
                                  {storeListOptions}
                                </Input>
                              </InputGroup>
                            </div>
                          </FormGroup>
                        </div>
                        <Button className='btn btn-success' type='submit'>
                          Guardar
                        </Button>
                        <Link to='..' className='btn btn-warning'>
                          Cancelar
                        </Link>
                      </Form>
                    </CardBody>
                  </Card>
                </div>
              </Row>
            )}
          </Formik>
        </Container>
      </>
    );
  }
}
export default PersonComponent;
