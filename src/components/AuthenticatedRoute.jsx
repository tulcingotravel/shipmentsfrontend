import React, {Component} from 'react'
import {Redirect, Route} from "react-router-dom";
import AuthenticationService from "../api/config/AuthenticationService.js";

class AuthenticatedRoute extends Component {
    render() {
        if (AuthenticationService.isUserLoggedIn()) {
            return <Route {...this.props}></Route>
        } else {
            return <Redirect to="/auth/login"></Redirect>
        }
    }
}

export default AuthenticatedRoute