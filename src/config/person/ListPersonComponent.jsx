import React, { Component } from 'react';
import PersonDataService from '../../api/config/PersonDataService.js';
import Header from '../../components/Headers/Header.js';
import {
  Button,
  Row,
  Card,
  CardHeader,
  CardFooter,
  Table,
  Container,
  Col
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import AuthenticationService from '../../api/config/AuthenticationService.js';
import Paginations from '../../components/Pages';
import '../../assets/css/Pages.scss';

class ListPersonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      message: null,
      allPersons: [],
      currentPersons: [],
      currentPage: null,
      totalPersons: null
    };
    this.refreshPersons = this.refreshPersons.bind(this);
    this.addPersonClicked = this.addPersonClicked.bind(this);
    this.updatePersonClicked = this.updatePersonClicked.bind(this);
    this.deletePersonClicked = this.deletePersonClicked.bind(this);
  }

  componentDidMount() {
    this.refreshPersons();
  }

  onPageChanged = (data) => {
    const { allPersons } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentPersons = allPersons.slice(offset, offset + pageLimit);
    this.setState({ currentPage, currentPersons, totalPages });
  };

  refreshPersons() {
    AuthenticationService.setupAxiosInterceptors();
    PersonDataService.findAll().then((response) => {
      this.setState({ allPersons: response.data });
    });
  }

  addPersonClicked() {
    this.props.history.push(`/persons/-1`);
  }

  updatePersonClicked(id) {
    this.props.history.push(`/persons/${id}`);
  }

  deletePersonClicked(id) {
    PersonDataService.deletePerson(id).then((response) => {
      this.setState({
        message: `Delete of person ${id} success`
      });
      this.refreshPersons();
    });
  }

  render() {
    const { allPersons, currentPersons, currentPage, totalPages } = this.state;
    const totalPersons = allPersons.length;
    if (totalPersons === 0) return null;

    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Card className='shadow'>
            <CardHeader className='border-0'>
              <h3 className='mb-0'>Personas</h3>
              <Row className='justify-content-between'>
                <Col className='col-6 d-flex justify-content-start'>
                  <Link
                    className='btn btn-primary'
                    role='button'
                    onClick={() => this.addPersonClicked}
                    to={'personas/-1'}
                  >
                    Nuevo
                  </Link>
                </Col>
                {currentPage && (
                  <Col className='col-6 d-flex justify-content-end'>
                    Página {currentPage} / {totalPages}
                  </Col>
                )}
              </Row>
            </CardHeader>
            <Table className='align-items-center table-flush' responsive>
              <thead className='thead-light'>
                <tr>
                  <th scope='col'>Nombre</th>
                  <th scope='col'>Apellidos</th>
                  <th scope='col'>Nacimiento</th>
                  <th scope='col'>Teléfono</th>
                  <th scope='col'>Correo</th>
                  <th scope='col'>Activo</th>
                  <th scope='col'>Actualizar</th>
                  <th scope='col'>Eliminar</th>
                </tr>
              </thead>
              <tbody>
                {currentPersons.map((person) => (
                  <tr key={person.id}>
                    <td>{person.name}</td>
                    <td>
                      {person.firstName} {person.lastName}
                    </td>
                    <td>{person.birthdate}</td>
                    <td>{person.phone}</td>
                    <td>{person.mail}</td>
                    <td>{person.active ? 'Activo' : 'No activo'}</td>
                    <td>
                      <Link
                        className='btn btn-success'
                        role='button'
                        to={`/admin/persons/${person.id}`}
                        onClick={() => this.updatePersonClicked(person.id)}
                      >
                        <FontAwesomeIcon icon='edit' />
                      </Link>
                    </td>
                    <td>
                      <Button
                        color='danger'
                        onClick={() => this.deletePersonClicked(person.id)}
                      >
                        <FontAwesomeIcon icon='trash' />
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <CardFooter className='py-4'>
              <nav aria-label='...'>
                <div className='d-flex flex-row py-4 align-items-center'>
                  <Paginations
                    totalRecords={totalPersons}
                    pageLimit={10}
                    pageNeighbours={1}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </nav>
            </CardFooter>
          </Card>
        </Container>
      </>
    );
  }
}

export default ListPersonComponent;
