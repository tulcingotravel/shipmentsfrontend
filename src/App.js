import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AuthenticatedRoute from './components/AuthenticatedRoute.jsx';
import LoginComponent from './components/LoginComponent.jsx';
import ListPersonComponent from './config/person/ListPersonComponent.jsx';
import PersonComponent from './config/person/PersonComponent.jsx';
import ListShipmentComponent from './config/shipments/ListShipmentComponent.jsx';
import ListShipmentsReportComponent from './config/shipments/ListShipmentsReportComponent.jsx';
import ShipmentsReportComponent from './config/shipments/ShipmentsReportComponent.jsx';
import WelcomeComponent from './components/WelcomeComponent.jsx';
import {library } from '@fortawesome/fontawesome-svg-core';
import {faEnvelope,faKey,faBell,faPowerOff,faTools,faUser,faSearch,faUsers,faUserPlus,faBoxOpen,faBoxes,faListAlt
} from '@fortawesome/free-solid-svg-icons';
library.add(faEnvelope,faKey,faBell,faPowerOff,faTools,faUser,faSearch,faUsers,faUserPlus,faBoxOpen,faBoxes,faListAlt);

function App() {
  return (
    <div className='App'>
      <Router>
        <Switch>
          <Route path='/' exact component={LoginComponent}></Route>
          <Route path='/login' component={LoginComponent}></Route>
          <AuthenticatedRoute
            path='/welcome/:name'
            component={WelcomeComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/persons/:id'
            component={PersonComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/persons'
            component={ListPersonComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/shipments/:id'
            component={ShipmentComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/shipments'
            component={ListShipmentComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/report-shipments/:id'
            component={ShipmentsReportComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/report/shipments'
            component={ListShipmentsReportComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/stores/:id'
            component={StoreComponent}
          ></AuthenticatedRoute>
          <AuthenticatedRoute
            path='/stores'
            component={ListStoreComponent}
          ></AuthenticatedRoute>          
          <AuthenticatedRoute
            path='/logout'
            component={LogoutComponent}
          ></AuthenticatedRoute>
        </Switch>       
      </Router>
    </div>
  );
}

export default App;
