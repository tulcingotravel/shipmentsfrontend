import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';
import reportWebVitals from 'reportWebVitals';
import "assets/scss/argon-dashboard-react.scss";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import AdminLayout from "layouts/Admin.js";
import AuthLayout from "layouts/Auth.js";
import AuthenticatedRoute from './components/AuthenticatedRoute.jsx'
import { library } from '@fortawesome/fontawesome-svg-core';
import {faEnvelope,faKey,faBell,faPowerOff,faTools,faUser,faSearch,faUsers,faUserPlus,faBoxOpen,faBoxes,
        faEdit,faTrash,faAngleLeft,faAngleRight,faHashtag,faWeight,faDollarSign,faPhone, faAddressCard, faMapMarkedAlt, 
        faBirthdayCake,faShippingFast,faBox,faStoreAlt,faReceipt, faBarcode, faFlag,faListAlt} from '@fortawesome/free-solid-svg-icons';
library.add(faEnvelope,faKey,faBell,faPowerOff,faTools,faUser,faSearch,faUsers,faUserPlus,faBoxOpen,faBoxes,
        faEdit,faTrash,faAngleLeft,faAngleRight,faHashtag,faWeight,faDollarSign,faPhone,faAddressCard,faMapMarkedAlt,
        faBirthdayCake,faShippingFast,faBox,faStoreAlt,faReceipt,faBarcode,faFlag,faListAlt);

ReactDOM.render(
  <BrowserRouter>
    <Switch>             
      <AuthenticatedRoute path="/admin" render={props => <AdminLayout {...props} />} />
      <Route path="/auth" render={props => <AuthLayout {...props} />} />           
      <Redirect from="/" to="/admin/index" />
      {/*
      import AuthenticatedRoute from './components/AuthenticatedRoute.jsx'
      import ShipmentComponent from "./config/shipments/ShipmentComponent.jsx";
      import LogoutComponent from './components/LogoutComponent.jsx'
      <Redirect from="/logout" to="/auth/login" />
      <AuthenticatedRoute path="/logout" component={LogoutComponent}></AuthenticatedRoute>
      <AuthenticatedRoute path="/shipments/:id" component={ShipmentComponent}></AuthenticatedRoute>
      <Redirect from='/shipments/:id' to="/auth/login" />*/}
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
