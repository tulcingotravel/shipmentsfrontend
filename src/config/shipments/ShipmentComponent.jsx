import { Component } from 'react';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import ShipmentDataService from '../../api/config/ShipmentDataService';
import Header from '../../components/Headers/Header.js';
import StoreDataService from '../../api/config/StoreDataService';
import {Button,FormGroup,InputGroup,InputGroupAddon,InputGroupText,Container,Row,Col,Card,CardHeader,CardBody,
  Label,Input,UncontrolledAlert} from 'reactstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AuthenticationService from '../../api/config/AuthenticationService.js';

class ShipmentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      shipmentNumber: '',
      originLocation: '',
      destinationLocation: '',
      senderPerson: '',
      senderPersonPhone: '',
      senderAddress: '',
      receiverPerson: '',
      receiverPersonPhone: '',
      receiverAddress: '',
      shipmentContent: '',
      sub: '',
      weight: '',
      cost: '',
      vEstimated: '',
      tax: '',
      delivered: false,
      locationOptions: [],
      originOptions: [],
      destinationOptions: [],
      error: false,
      folio:'',
      quantity:1
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
    this.inputChangedHandler = this.inputChangedHandler.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  onSubmit(values) {
    let shipment = {
      id: this.state.id,
      shipmentNumber: values.shipmentNumber,
      originLocation: values.originLocation,
      destinationLocation: values.destinationLocation,
      senderPerson: values.senderPerson,
      senderPersonPhone: values.senderPersonPhone,
      senderAddress: values.senderAddress,
      receiverPerson: values.receiverPerson,
      receiverPersonPhone: values.receiverPersonPhone,
      receiverAddress: values.receiverAddress,
      shipmentContent: values.shipmentContent,
      sub: values.sub,
      weight: values.weight,
      cost: values.cost,
      vEstimated: values.vEstimated,
      tax: values.tax,
      delivered: values.delivered,
      quantity: values.quantity
    };  
    if (this.state.id === -1) {
      if(shipment.originLocation === null){
        shipment.originLocation = this.state.originOptions[0].props.value;
      }
      if(shipment.destinationLocation === null){
        shipment.destinationLocation = this.state.destinationOptions[0].props.value;
      }
      if(shipment.delivered === null){
        shipment.delivered = false
      }
      ShipmentDataService.createShipment(shipment).then(() =>
        this.props.history.push('/shipments')
      );
    } else {
      ShipmentDataService.updateShipment(this.state.id, shipment).then(() =>
        this.props.history.push('/shipments')
      );
    }
  }

  validate(values) {
    let errors = {};
    if (!values.senderPerson) {
      errors.senderPerson = 'Persona emisora obligatorio.';
    } else if (!values.receiverPerson) {
      errors.receiverPerson = 'Persona receptora obligtorio.';
    } else if (!values.receiverPerson) {
      errors.receiverPerson = 'Número de envío obligtorio.';
    } else if (!values.receiverPersonPhone) {
      errors.receiverPerson = 'Teléfono de persona receptora obligtorio.';
    } else if (!values.senderPersonPhone) {
      errors.receiverPerson = 'Teléfono de persona emisora obligtorio.';
    } else if (!values.shipmentContent) {
      errors.receiverPerson = 'Contenido del envío obligtorio.';
    }
    return errors;
  }

  componentDidMount() {
    AuthenticationService.setupAxiosInterceptors();
    //Review data initialization
    const {
      id,
      shipmentNumber,
      originLocation,
      destinationLocation,
      senderPerson,
      senderPersonPhone,
      senderAddress,
      receiverPerson,
      receiverPersonPhone,
      receiverAddress,
      shipmentContent,
      sub,
      weight,
      cost,
      vEstimated,
      tax,
      delivered,
      locationOptions,
      originOptions,
      destinationOptions,
      folio,
      quantity
    } = this.props;
    this.setState({
      id,
      shipmentNumber,
      originLocation,
      destinationLocation,
      senderPerson,
      senderPersonPhone,
      senderAddress,
      receiverPerson,
      receiverPersonPhone,
      receiverAddress,
      shipmentContent,
      sub,
      weight,
      cost,
      vEstimated,
      tax,
      delivered,
      locationOptions,
      originOptions,
      destinationOptions,
      folio,
      quantity
    });
    //End review
    if (this.state.id === -1 || this.state.id === null) {
      return;
    }

    var origin;
    var destination;
    var countryOrigin = localStorage.getItem('country');
    if (countryOrigin === 'Mexico') {
      origin = 'Mexico';
      destination = 'USA';
    } else {
      origin = 'USA';
      destination = 'Mexico';
    }

    StoreDataService.findAll().then((response) => {
      var locations = response.data;
      this.setState({
        originOptions: (locations || [])
          .filter((location) => location.country === origin)
            .sort(ShipmentDataService.orderStoresByNameAsc)
          .map((location, index) => (
            <option value={location.id} id={index}>{location.name}</option>
          )),
        destinationOptions: (locations || [])
          .filter((location) => location.country === destination)
            .sort(ShipmentDataService.orderStoresByNameAsc)
          .map((location, index) => (
            <option value={location.id}>{location.name}</option>
          ))
      });
    });

    ShipmentDataService.findShipmentById(this.state.id)
      .then((response) =>{
        this.setState({
          id: response.data.id === null ? this.props.match.params.id : response.data.id,
          shipmentNumber: response.data.shipmentNumber,
          originLocation: response.data.originLocation.id,
          destinationLocation: response.data.destinationLocation.id,
          senderPerson: response.data.senderPerson,
          senderPersonPhone: response.data.senderPersonPhone,
          senderAddress: response.data.senderAddress,
          receiverPerson: response.data.receiverPerson,
          receiverPersonPhone: response.data.receiverPersonPhone,
          receiverAddress: response.data.receiverAddress,
          shipmentContent: response.data.shipmentContent,
          sub: response.data.sub,
          weight: response.data.weight,
          cost: response.data.cost,
          vEstimated: response.data.vEstimated,
          tax: response.data.tax,
          delivered: response.data.delivered,
          folio: response.data.originLocation.folio,
          quantity: response.data.quantity
        })
      }
      )
      .catch((error) => {
        this.setState({
          error: true
        });
      });
      StoreDataService.findStoreById(localStorage.getItem('store'))
      .then((response) => {
        if (this.state.id === -1) {
          this.setState({
            shipmentNumber: response.data.folioCounter,
          });
        }

      });      

  }

  inputChangedHandler(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  render() {
    let {
      id,
      shipmentNumber,
      originLocation,
      destinationLocation,
      senderPerson,
      senderPersonPhone,
      senderAddress,
      receiverPerson,
      receiverPersonPhone,
      receiverAddress,
      shipmentContent,
      sub,
      weight,
      cost,
      vEstimated,
      tax,
      delivered,
      originOptions,
      destinationOptions,
      folio,
      quantity
    } = this.state;
    return (
      <>
        <Header />
        <Formik
          initialValues={{
            id,
            shipmentNumber,
            originLocation,
            destinationLocation,
            senderPerson,
            senderPersonPhone,
            senderAddress,
            receiverPerson,
            receiverPersonPhone,
            receiverAddress,
            shipmentContent,
            sub,
            weight,
            cost,
            vEstimated,
            tax,
            delivered,
            folio,
            quantity
          }}
          onSubmit={this.onSubmit}
          validate={this.validate}
          enableReinitialize={true}
        >
          {(props) => (
            <Container className='mt--7' fluid>
              {this.state.error && (
                <UncontrolledAlert color='danger'>
                  {' '}
                  No puedes acceder, no cuentas con permisos suficientes.{' '}
                </UncontrolledAlert>
              )}
              <Form className='row'>
                <Col className='order-xl-2 mb-5 mb-xl-0' xl='4'>
                  <Card className='bg-secondary shadow'>
                    <CardHeader className='bg-white text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4'>
                      <h1>Adicional</h1>
                    </CardHeader>
                    <CardBody className='pt-0 pt-md-4'>
                      <Row>
                        <div className='col'>
                          <Label>No.</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                {this.state.folio} 
                                <FontAwesomeIcon icon='hashtag' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='shipmentNumber'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <hr className='my-4' />
                      <Row>
                        <div className='col'>
                          <Label>Cantidad</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                <FontAwesomeIcon icon='hashtag' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='quantity'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <hr className='my-4' />
                      <Row>
                        <div className='col'>
                          <Label>Sub</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                <FontAwesomeIcon icon='hashtag' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='sub'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <hr className='my-4' />
                      <Row>
                        <div className='col'>
                          <Label>Peso</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                <FontAwesomeIcon icon='weight' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='weight'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <hr className='my-4' />
                      <Row>
                        <div className='col'>
                          <Label>Costo</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                <FontAwesomeIcon icon='dollar-sign' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='cost'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <hr className='my-4' />
                      <Row>
                        <div className='col'>
                          <Label>V. Estimado</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                <FontAwesomeIcon icon='dollar-sign' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='vEstimated'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <hr className='my-4' />
                      <Row>
                        <div className='col'>
                          <Label>Impuesto</Label>
                          <InputGroup>
                            <InputGroupAddon addonType='prepend'>
                              <InputGroupText>
                                <FontAwesomeIcon icon='dollar-sign' />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Field
                              className='form-control'
                              type='text'
                              name='tax'
                              onChange={this.inputChangedHandler}
                            />
                          </InputGroup>
                        </div>
                      </Row>
                      <div className='text-center'></div>
                    </CardBody>
                  </Card>
                </Col>
                <Col className='order-xl-1' xl='8'>
                  <Card className='bg-secondary shadow'>
                    <CardHeader className='bg-white border-0'>
                      <Row className='align-items-center'>
                        <h3 className='mb-0'>Ingresa los datos</h3>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <ErrorMessage
                        name='senderPerson'
                        component='div'
                        className='alert alert-warning'
                      />
                      <ErrorMessage
                        name='receiverPerson'
                        component='div'
                        className='alert alert-warning'
                      />
                      {/* Sender Person */}
                      <h6 className='heading-small text-muted mb-4'>
                        PERSONA QUE ENVIA
                      </h6>
                      <div className='pl-lg-4'>
                        <FormGroup className='row'>
                          <div className='col-md-12'>
                            <Label>Emisor</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='user' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Field
                                className='form-control'
                                type='text'
                                name='senderPerson'
                                onChange={this.inputChangedHandler}
                              />
                            </InputGroup>
                          </div>
                        </FormGroup>
                        <FormGroup className='row'>
                          <div className='col-md-4'>
                            <Label>Teléfono</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='phone' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Field
                                className='form-control'
                                type='text'
                                name='senderPersonPhone'
                                onChange={this.inputChangedHandler}
                              />
                            </InputGroup>
                          </div>
                          <div className='col-md-8'>
                            <Label>Dirección emisor</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='address-card' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Field
                                className='form-control'
                                type='text'
                                name='senderAddress'
                                onChange={this.inputChangedHandler}
                              />
                            </InputGroup>
                          </div>
                        </FormGroup>
                      </div>
                      <hr className='my-4' />
                      {/* Receiver Person */}
                      <h6 className='heading-small text-muted mb-4'>
                        PERSONA QUE RECIBE
                      </h6>
                      <div className='pl-lg-4'>
                        <FormGroup className='row'>
                          <div className='col-md-12'>
                            <Label>Receptor</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='user' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Field
                                className='form-control'
                                type='text'
                                name='receiverPerson'
                                onChange={this.inputChangedHandler}
                              />
                            </InputGroup>
                          </div>
                        </FormGroup>
                        <FormGroup className='row'>
                          <div className='col-md-4'>
                            <Label>Teléfono</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='phone' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Field
                                className='form-control'
                                type='text'
                                name='receiverPersonPhone'
                                onChange={this.inputChangedHandler}
                              />
                            </InputGroup>
                          </div>
                          <div className='col-md-8'>
                            <Label>Dirección emisor</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='address-card' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Field
                                className='form-control'
                                type='text'
                                name='receiverAddress'
                                onChange={this.inputChangedHandler}
                              />
                            </InputGroup>
                          </div>
                        </FormGroup>
                      </div>
                      <hr className='my-4' />
                      {/* Shipment Description */}
                      <h6 className='heading-small text-muted mb-4'>
                        INFORMACIÓN DEL PAQUETE
                      </h6>
                      <div className='pl-lg-4'>
                        <FormGroup className='row'>
                          <div className='col-md-6'>
                            <Label>Lugar origen</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='map-marked-alt' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                className='form-control'
                                type='select'
                                name='originLocation'
                                value={this.state.originLocation}
                                onChange={this.inputChangedHandler}
                              >
                                options={originOptions}
                              </Input>
                            </InputGroup>
                          </div>
                          <div className='col-md-6'>
                            <Label>Lugar destino</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <InputGroupText>
                                  <FontAwesomeIcon icon='map-marked-alt' />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                className='form-control'
                                type='select'
                                name='destinationLocation'
                                value={this.state.destinationLocation}
                                onChange={this.inputChangedHandler}
                              >
                                options={destinationOptions}
                              </Input>
                            </InputGroup>
                          </div>
                        </FormGroup>
                        <FormGroup className='row'>
                          <div className='col-md-12'>
                            <Label>Contenido</Label>
                            <div className='input-group'>
                              <div className='input-group-prepend'>
                                <span className='input-group-text'>
                                  <FontAwesomeIcon icon='box-open' />
                                </span>
                              </div>
                              <textarea
                                className='form-control'
                                aria-label='Contenido'
                                name='shipmentContent'
                                value={this.state.shipmentContent}
                                onChange={this.inputChangedHandler}
                                rows='4'
                              />
                            </div>
                          </div>
                        </FormGroup>
                        {!(this.state.id === undefined) && (
                          <FormGroup className='row'>
                            <div className='col'>
                              <FormGroup check>
                                <Label check>
                                  <Input
                                    type='checkbox'
                                    name='delivered'
                                    checked={this.state.delivered}
                                    onChange={this.handleInputChange}
                                  />
                                  Entregado
                                </Label>
                              </FormGroup>
                            </div>
                          </FormGroup>
                        )}
                      </div>
                      <Button className='btn btn-success' type='submit'>
                        Guardar
                      </Button>
                      <Link to='..' className='btn btn-warning'>
                        Cancelar
                      </Link>
                    </CardBody>
                  </Card>
                </Col>
              </Form>
            </Container>
          )}
        </Formik>
      </>
    );
  }
}
export default ShipmentComponent;
