import axios from "axios";
import { API_URL } from "../../Constants";

class StoreDataService{

    findAll(){
        return axios.get(`${API_URL}/config/store/`);
    }

    findStoreById(id){
        return axios.get(`${API_URL}/config/store/${id}`);
    }
    
    updateStore(id, store){
        return axios.put(`${API_URL}/config/store/${id}`, store);
    }

    createStore(store){
        return axios.post(`${API_URL}/config/store/`, store);
    }

    deleteStore(id){
        return axios.delete(`${API_URL}/config/store/${id}`);
    }
}

export default new StoreDataService()