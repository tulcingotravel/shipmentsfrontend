import axios from "axios";
import { API_URL } from "../../Constants";

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
export const USER_TOKEN_SESSION = 'token'
class AuthenticationService {

    executeJwtAuthenticationService(username, password){
        return axios.post(`${API_URL}/authenticate`, 
         { username, password})
     }

     
    findPersonByUsername(username){
        return axios.get(`${API_URL}/config/person/username/${username}`);
    }

    registerSuccessfulLoginForJwt(user) {
        localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, user.username);
        localStorage.setItem("role", user.role);
        this.createJwtToken(user.token);        
        this.setupAxiosInterceptors();
    }

    createJwtToken(token){
        let authenticationToken = 'Bearer ' + token
        localStorage.setItem(USER_TOKEN_SESSION, authenticationToken)
        return authenticationToken
    }

    logout(){
        localStorage.clear();        
    }

    isUserLoggedIn() {
        let user = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return false;
        return true;
    }

    getLoggedInUserName() {
        let user = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return '';
        return user;
    }

    setupAxiosInterceptors(){
        
        axios.interceptors.request.use(
            async (config) => {
                if(this.isUserLoggedIn()){
                    config.headers.Authorization = localStorage.getItem(USER_TOKEN_SESSION)
                }
                return config
            },
            (error) => {
                return Promise.reject(error)
            }
        )
    }
}

export default new AuthenticationService()