import { Component } from 'react';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import StoreDataService from '../../api/config/StoreDataService';
import Header from '../../components/Headers/Header.js';
import {
  Button,
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Card,
  CardHeader,
  CardBody,
  Container
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class StoreComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      name: '',
      address: '',
      phone: '',
      country: 'México',
      folio: '',
      active: false,
      folioCounter:''
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
    this.inputChangedHandler = this.inputChangedHandler.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  onSubmit(values) {
    let store = {
      id: this.state.id,
      name: values.name,
      address: values.address,
      phone: values.phone,
      country: values.country,
      folio: values.folio,
      active: values.active,
      folioCounter: values.folioCounter
    };
    if (this.state.id === -1 || this.state.id === undefined) {
      StoreDataService.createStore(store).then(() =>
        this.props.history.push('/stores')
      );
    } else {
      StoreDataService.updateStore(this.state.id, store).then(() =>
        this.props.history.push('/stores')
      );
    }
  }

  validate(values) {
    let errors = {};
    if (!values.name) {
      errors.name = 'Nombre de tienda obligatorio.';
    }
    return errors;
  }

  componentDidMount() {
    //Review data initialization
    const { id, name, address, phone, country, folio, active, folioCounter } = this.props;
    this.setState({ id, name, address, phone, country, folio, active, folioCounter });
    //End review
    if (this.state.id === -1) {
      return;
    }

    StoreDataService.findStoreById(this.state.id).then((response) =>
      this.setState({
        id: response.data.id,
        name: response.data.name,
        address: response.data.address,
        phone: response.data.phone,
        country: response.data.country,
        folio: response.data.folio,
        active: response.data.active,
        folioCounter: response.data.folioCounter
      })
    );
  }

  inputChangedHandler(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    let { id, name, address, phone, country, folio, active, folioCounter } = this.state;
    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Formik
            initialValues={{ id, name, address, phone, country, folio, active, folioCounter }}
            onSubmit={this.onSubmit}
            validate={this.validate}
            enableReinitialize={true}
          >
            {(props) => (
              <Row>
                <div className='col'>
                  <Card className='shadow'>
                    <CardHeader className='bg-white border-0'>
                      <Row className='align-items-center'>
                        <h3 className='mb-0'>Ingresa los datos</h3>
                      </Row>
                    </CardHeader>
                    <CardBody>
                      <Form>
                        <ErrorMessage
                          name='name'
                          component='div'
                          className='alert alert-warning'
                        />
                        <ErrorMessage
                          name='country'
                          component='div'
                          className='alert alert-warning'
                        />
                        {/* Main info */}
                        <h6 className='heading-small text-muted mb-4'>
                          DATOS PRINCIPALES
                        </h6>
                        <div className='pl-lg-4'>
                          <FormGroup className='row'>
                            <div className='col-md-4'>
                              <Label>Nombre</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='user' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='name'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-4'>
                              <Label>Dirección</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='address-card' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='address'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-4'>
                              <Label>phone</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='phone' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='phone'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                          </FormGroup>
                        </div>
                        <hr className='my-4' />
                        {/* Optional info */}
                        <h6 className='heading-small text-muted mb-4'>
                          INFORMACIÓN OPCIONAL
                        </h6>
                        <div className='pl-lg-4'>
                          <FormGroup className='row'>
                            <div className='col-md-4'>
                              <Label>País</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='flag' />
                                  </InputGroupText>
                                </InputGroupAddon>                                
                                <Input
                                  className='form-control'
                                  type='select'
                                  name='country'
                                  value={this.state.country}
                                  onChange={this.inputChangedHandler}
                                >
                                  <option value='México'>México</option>
                                  <option value='USA' selected>USA</option>
                                </Input>
                              </InputGroup>
                            </div>
                            <div className='col-md-4'>
                              <Label>Folio</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='weight' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  className='form-control'
                                  type='text'
                                  name='folio'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                            <div className='col-md-4'>
                              <Label>Contador</Label>
                              <InputGroup>
                                <InputGroupAddon addonType='prepend'>
                                  <InputGroupText>
                                    <FontAwesomeIcon icon='weight' />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Field
                                  //disabled = {true}
                                  className='form-control'
                                  type='text'
                                  name='folioCounter'
                                  onChange={this.inputChangedHandler}
                                />
                              </InputGroup>
                            </div>
                          </FormGroup>
                        </div>
                        <hr className='my-4' />
                        {/* Access information */}
                        <div className='pl-lg-4'>
                          <FormGroup className='row'>
                            <div className='col'>
                              <FormGroup check>
                                <Label check>
                                  <Input
                                    type='checkbox'
                                    name='active'
                                    checked={this.state.active}
                                    onChange={this.handleInputChange}
                                  />
                                  Activo
                                </Label>
                              </FormGroup>
                            </div>
                          </FormGroup>
                        </div>
                        <Button className='btn btn-success' type='submit'>
                          Guardar
                        </Button>
                        <Link to='..' className='btn btn-warning'>
                          Cancelar
                        </Link>
                      </Form>
                    </CardBody>
                  </Card>
                </div>
              </Row>
            )}
          </Formik>
        </Container>
      </>
    );
  }
}
export default StoreComponent;
