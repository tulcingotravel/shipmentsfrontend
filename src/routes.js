/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import ListPersonComponent from "./config/person/ListPersonComponent.jsx";
import ListShipmentComponent from "./config/shipments/ListShipmentComponent.jsx";
import ListShipmentsReportComponent from "./config/shipments/ListShipmentsReportComponent";
import ListStoreComponent from "./config/location/ListStoreComponent.jsx";
import ShipmentComponent from "./config/shipments/ShipmentComponent.jsx";
import PersonComponent from "./config/person/PersonComponent.jsx";
import StoreComponent from "./config/location/StoreComponent.jsx";

var routes = [
  {
    path: "/index",
    name: "Paquetes",
    icon: "boxes",
    className: "text-info",
    component: ListShipmentComponent,
    layout: "/admin",
    role:[1,2,3]
  },
  {
    path: "/shipments/-1",
    name: "Agregar Paquete",
    icon: "box-open",
    className: "text-warning",
    component: ShipmentComponent,
    layout: "/admin",
    role:[1,3]
  },
  {
    path: "/personas/-1",
    name: "Agregar Persona",
    icon: "user",
    className: "text-primary",
    component: PersonComponent,
    layout: "/admin",
    role:[1]
  },
  {
    path: "/personas",
    name: "Personas",
    icon: "users",
    className: "text-success",
    component: ListPersonComponent,
    layout: "/admin",
    role:[1,2,3]
  },
  {
    path: "/stores",
    name: "Tiendas",
    icon: "store-alt",
    className: "text-danger",
    component: ListStoreComponent,
    layout: "/admin",
    role:[1,2]
  },
  {
    path: "/stores/-1",
    name: "Agregar Tienda",
    icon: "store-alt",
    className: "text-danger",
    component: StoreComponent,
    layout: "/admin",
    role:[1,2]
  },
  {
    path: "/report/shipments",
    name: "Listas",
    icon: "list-alt",
    className: "text-primary",
    component: ListShipmentsReportComponent,
    layout: "/admin",
    role:[1,2,3]
  }
];
export default routes;
