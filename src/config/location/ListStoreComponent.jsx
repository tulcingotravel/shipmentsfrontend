import React, { Component } from 'react';
import StoreDataService from '../../api/config/StoreDataService.js';
import Header from '../../components/Headers/Header.js';
import {
  Button,
  Row,
  Col,
  Card,
  CardHeader,
  CardFooter,
  Table,
  Container
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import AuthenticationService from '../../api/config/AuthenticationService.js';
import Paginations from '../../components/Pages';
import '../../assets/css/Pages.scss';

class ListStoreComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stores: [],
      message: null,
      allStores: [],
      currentStores: [],
      currentPage: null,
      totalStores: null
    };
    this.refreshStores = this.refreshStores.bind(this);
    this.addStoreClicked = this.addStoreClicked.bind(this);
    this.updateStoreClicked = this.updateStoreClicked.bind(this);
    this.deleteStoreClicked = this.deleteStoreClicked.bind(this);
  }

  componentDidMount() {
    this.refreshStores();
  }

  onPageChanged = (data) => {
    const { allStores } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentStores = allStores.slice(offset, offset + pageLimit);
    this.setState({ currentPage, currentStores, totalPages });
  };

  refreshStores() {
    AuthenticationService.setupAxiosInterceptors();
    StoreDataService.findAll().then((response) => {
      this.setState({ allStores: response.data });
    });
  }

  addStoreClicked() {
    this.props.history.push(`/stores/-1`);
  }

  updateStoreClicked(id) {
    this.props.history.push(`/stores/${id}`);
  }

  deleteStoreClicked(id) {
    StoreDataService.deleteStore(id).then((response) => {
      this.setState({
        message: `Delete of store ${id} success`
      });
      this.refreshStores();
    });
  }

  render() {
    const { allStores, currentStores, currentPage, totalPages } = this.state;
    const totalStores = allStores.length;
    if (totalStores === 0) return null;

    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Card className='shadow'>
            <CardHeader className='border-0'>
              <h3 className='mb-0'>Sucursales</h3>
              <Row className='justify-content-between'>
                <Col className='col-6 d-flex justify-content-start'>
                  <Link
                    className='btn btn-primary'
                    role='button'
                    to={'/admin/stores/-1'}
                    onClick={() => this.addStoreClicked}
                  >
                    Nuevo
                  </Link>
                </Col>
                {currentPage && (
                  <Col className='col-6 d-flex justify-content-end'>
                    Página {currentPage} / {totalPages}
                  </Col>
                )}
              </Row>
            </CardHeader>
            <Table className='align-items-center table-flush' responsive>
              <thead className='thead-light'>
                <tr>
                  <th scope='col'>Nombre</th>
                  <th scope='col'>Dirección</th>
                  <th scope='col'>Teléfono</th>
                  <th scope='col'>País</th>
                  <th scope='col'>Tipo</th>
                  <th scope='col'>Activo</th>
                  <th scope='col'>Acciones</th>
                </tr>
              </thead>
              <tbody>
                {currentStores.map((store) => (
                  <tr key={store.id}>
                    <td>{store.name}</td>
                    <td>{store.address}</td>
                    <td>{store.phone}</td>
                    <td>{store.country}</td>
                    <td>{store.type}</td>
                    <td>{store.active ? 'Activo' : 'No activo'}</td>
                    <td>
                      <Link
                        className='btn btn-success'
                        role='button'
                        to={`/admin/stores/${store.id}`}
                        onClick={() => this.updateStoreClicked(store.id)}
                      >
                        <FontAwesomeIcon icon='edit' />
                      </Link>
                      <Button
                        color='danger'
                        onClick={() => this.deleteStoreClicked(store.id)}
                      >
                        <FontAwesomeIcon icon='trash' />
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <CardFooter className='py-4'>
              <nav aria-label='...'>
                <div className='d-flex flex-row py-4 align-items-center'>
                  <Paginations
                    totalRecords={totalStores}
                    pageLimit={10}
                    pageNeighbours={1}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </nav>
            </CardFooter>
          </Card>
        </Container>
      </>
    );
  }
}

export default ListStoreComponent;
