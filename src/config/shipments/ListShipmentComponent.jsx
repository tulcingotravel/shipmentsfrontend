import { Component } from 'react';
import ShipmentDataService from '../../api/config/ShipmentDataService.js';
import Header from '../../components/Headers/Header.js';
import {Badge,Button,Row,Card,CardHeader,CardFooter,Table,Container,Col} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import AuthenticationService from '../../api/config/AuthenticationService.js';
import Paginations from '../../components/Pages';
import '../../assets/css/Pages.scss';

class ListShipmentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shipments: [],
      message: null,
      allShipments: [],
      currentShipments: [],
      currentPage: null,
      totalShipments: null
    };
    this.refreshShipments = this.refreshShipments.bind(this);
    this.addShipmentClicked = this.addShipmentClicked.bind(this);
    this.updateShipmentClicked = this.updateShipmentClicked.bind(this);
    this.deleteShipmentClicked = this.deleteShipmentClicked.bind(this);
    this.printLabelClicked = this.printLabelClicked.bind(this);
    this.printReceiptClicked = this.printReceiptClicked.bind(this);
    this.createShipmentReport = this.createShipmentReport.bind(this);
  }

  componentDidMount() {
    const search =this.props.location.search;
    const params = new URLSearchParams(search); 
    var filter = params.get('filter')
    this.refreshShipments(filter);
  }

  componentDidUpdate(prevProps) {
    
    if (prevProps.location.search !== this.props.location.search) {
      const search = this.props.location.search;
      const params = new URLSearchParams(search);       
      var filter = params.get('filter');
      this.refreshShipments(filter);
      //this.forceUpdate();
    }
  }

  onPageChanged = (data) => {
    const { allShipments } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentShipments = allShipments.slice(offset, offset + pageLimit);
    this.setState({ currentPage, currentShipments, totalPages });
  };

  refreshShipments(filterSearch) {    
    AuthenticationService.setupAxiosInterceptors();
    if(filterSearch === undefined || filterSearch === null){
      ShipmentDataService.findAll().then((response) => {
        this.setState({ allShipments: response.data });
      });
    }else{
      ShipmentDataService.findShipmentsBySenderOrReceiver(filterSearch).then((response) => {
        this.setState({ allShipments: response.data });
      });
    }
  }
  printLabelClicked(shipment) {
    var pri = document.getElementById('labeltoprint').contentWindow;
    var printContent =
      '<h1>' +
      shipment.shipmentNumber +
      '</h1> <h3>' +
      shipment.destinationLocation +
      '</h3> ' +
      '<h5>E: ' +
      shipment.senderPerson +
      '</h5>' +
      '<h5>R: ' +
      shipment.receiverPerson +
      '</h5>';
    pri.document.open();
    pri.document.write(printContent);
    pri.document.close();
    pri.focus();
    pri.print();
    pri.document.write('');
  }

  printReceiptClicked(shipment) {
    var pri = document.getElementById('labeltoprint').contentWindow;
    var printContent =
      "<div className='container'> 	<div className='row'> 		<table> 			<thead> 				<tr> 					<td> 						TULCINGO TRAVEL</br> 						85-16 Roosevelt Ave. 1fl., Jackson</br> 						Heights, NY 11372</br> 						Tel : 718 639-9153, 718 639-915</br> 					</td> 					<td> 						Paquete #:" +
      shipment.shipmentNumber +
      ' &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Acepto de Conformidad</br> 						Destino:' +
      shipment.destinationLocation +
      ' &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;        ________________________</br>' +
      shipment.receiverAddress +
      '</br>Tel: ' +
      shipment.receiverPersonPhone +
      '</br> ' +
      shipment.createdAt +
      '</br> 					</td> 				</tr> 			</thead> 			<tbody> <tr><b>* SALIENDO DE LA SUCURSAL, NO SE ACEPTAN RECLAMOS</b></tr>				<tr> 					<td> 						<h2>REMITENTE</h2> 						<p> 						  </br>NOMBRE:' +
      shipment.senderPerson +
      '</br>TEL: ' +
      shipment.senderPersonPhone +
      '</br>DIRECCIÓN: ' +
      shipment.senderAddress +
      '</p> 					</td> 					<td> 						<h2>BENEFICIARIO</h2> 						<p> 						  NOMBRE:' +
      shipment.receiverPerson +
      '</br>TEL: ' +
      shipment.receiverPersonPhone +
      '</br>DIRECCIÓN:' +
      shipment.receiverAddress +
      "</p> 					</td> 				</tr> 			</tbody> 		</table> 	</div> 	<div className='row'> 	  <table> 		<thead> 		  <tr> 			<td>PAQ.</td> 			<td>CONTENIDO</td> 			<td>SUBPAQ.</td> 			<td>$ EST.</td> 			<td>OTROS</td> 			<td>LBS.</td> 			<td>PRECIO</td> 			<td>TOTAL</td> 		  </tr> 		</thead> 		<tbody> 		  <tr> 			<td>" +
      shipment.shipmentNumber +
      '</td> 			<td>' +
      shipment.shipmentContent +
      '</td> 			<td>0</td> 			<td>' +
      shipment.vEstimated +
      '</td> 			<td>0</td> 			<td>' +
      shipment.weight +
      '</td> 			<td>' +
      shipment.cost +
      '</td> 			<td>' +
      shipment.cost +
      '</td> 		  </tr> 		</tbody> 	  </table> 	</div> 	<p> 	Nota: En caso de alguna perdida, se le devolverá el valor que usted declaro. La empresa no se hace responsable por artículos no declarados. Es estrictamente prohibido meter 	dinero en los sobres o paquetes, esto es un delito federal. La compañía no se hace responsable por paquetes no revisados por su familiar en MEXICO. 	GRACIAS POR SU COMPRENSION. 	</p> </div>';
    pri.document.open();
    pri.document.write(printContent);
    pri.document.close();
    pri.focus();
    pri.print();
    pri.document.write('');
  }

  addShipmentClicked() {
    this.props.history.push('/shipments/-1');
  }

  updateShipmentClicked(id) {
    this.props.history.push(`/shipments/${id}`);
  }

  deleteShipmentClicked(id) {
    ShipmentDataService.deleteShipment(id).then((response) => {
      this.setState({
        message: `Delete of shipment ${id} success`
      });
      this.refreshShipments();
    });
  }

  createShipmentReport() {
    ShipmentDataService.createShipmentReport().then(() => {
      this.props.history.push('/admin/report/shipments');
    });
  }

  render() {
    const {
      allShipments,
      currentShipments,
      currentPage,
      totalPages
    } = this.state;
    const totalShipments = allShipments.length;
    if (totalShipments === 0) return null;
    return (
      <>
        <Header />
        <Container className='mt--7' fluid>
          <Card className='shadow'>
            <CardHeader className='border-0'>
              <h3 className='mb-0'>Envíos</h3>
              <Row className='justify-content-between'>
                <Col className='col-6 d-flex justify-content-start'>
                  <Link
                    className='btn btn-primary'
                    role='button'
                    onClick={this.createShipmentReport}
                    //to={'shipments/-1'}
                  >
                    Generar Corte
                  </Link>
                </Col>
                {currentPage && (
                  <Col className='col-6 d-flex justify-content-end'>
                    Página {currentPage} / {totalPages}
                  </Col>
                )}
              </Row>
            </CardHeader>
            <Table className='align-items-center table-flush' responsive>
              <thead className='thead-light'>
                <tr>
                  <th scope='col'>Acciones</th>
                  <th scope='col'># Envío</th>
                  <th scope='col'>Entregado</th>
                  <th scope='col'>Destino</th>
                  <th scope='col'>Valor</th>
                  <th scope='col'>Libras</th>
                  <th scope='col'>Cobro</th>
                  <th scope='col'>Recibe</th>
                  <th scope='col'>Envía</th>
                  <th scope='col'>Contenido</th>
                  <th scope='col'>Fecha</th>
                  <th scope='col'>Etiquetas</th>
                </tr>
              </thead>
              <tbody>
                {currentShipments.map((shipment) => (
                  <tr key={shipment.id} id={shipment.id}>
                    <td>
                      <Link
                        className='btn btn-outline-success'
                        role='button'
                        to={`/admin/shipments/${shipment.id}`}
                        onClick={() => this.updateShipmentClicked(shipment.id)}
                      >
                        <FontAwesomeIcon icon='edit' />
                      </Link>
                      <Button
                        outline='true'
                        color='danger'
                        onClick={() => this.deleteShipmentClicked(shipment.id)}
                      >
                        <FontAwesomeIcon icon='trash' />
                      </Button>
                    </td>
                    <td>{shipment.shipmentNumber}</td>
                    <td>
                      {shipment.delivered ? (
                        <Badge color='' className='badge-dot mr-4'>
                          <i className='bg-success' />
                          Entregado
                        </Badge>
                      ) : (
                        <Badge color='' className='badge-dot mr-4'>
                          <i className='bg-danger' />
                          Sin entregar
                        </Badge>
                      )}
                    </td>
                    <td>{shipment.destinationLocation.name}</td>
                    <td>{shipment.vEstimated}</td>
                    <td>{shipment.weight}</td>
                    <td>{shipment.cost}</td>
                    <td>{shipment.receiverPerson}</td>
                    <td>{shipment.senderPerson}</td>
                    <td>{shipment.shipmentContent}</td>
                    <td>{shipment.createdAt}</td>
                    <td>
                      <Button
                        outline='true'
                        color='info'
                        onClick={() => this.printLabelClicked(shipment)}
                      >
                        <FontAwesomeIcon icon='barcode' />
                      </Button>
                      <Button
                        outline='true'
                        color='dark'
                        onClick={() => this.printReceiptClicked(shipment)}
                      >
                        <FontAwesomeIcon icon='receipt' />
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <CardFooter className='py-4'>
              <nav aria-label='...'>
                <div className='d-flex flex-row py-4 align-items-center'>
                  <Paginations
                    totalRecords={totalShipments}
                    pageLimit={10}
                    pageNeighbours={1}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </nav>
            </CardFooter>
          </Card>
        </Container>
        {/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
        <iframe id='labeltoprint'></iframe>
      </>
    );
  }
}

export default ListShipmentComponent;
