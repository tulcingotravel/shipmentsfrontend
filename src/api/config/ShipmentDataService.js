import axios from "axios";
import { API_URL } from "../../Constants";

class ShipmentDataService{

    findAll(){
        return axios.get(`${API_URL}/main/shipment/`);
    }

    findShipmentById(id){
        return axios.get(`${API_URL}/main/shipment/${id}`);
    }
    
    updateShipment(id, shipment){
        return axios.put(`${API_URL}/main/shipment/${id}`, shipment);
    }

    createShipment(shipment){
        return axios.post(`${API_URL}/main/shipment/`, shipment);
    }

    deleteShipment(id){
        return axios.delete(`${API_URL}/main/shipment/${id}`);
    }

    /**
     * 
     * @returns Shipments report
     */
    findAllShipmentReport(){
        return axios.get(`${API_URL}/main/report/shipment/`);
    }

    findAllShipmentReportById(shipmentReportId){
        return axios.get(`${API_URL}/main/report/shipment/${shipmentReportId}/detail`);
    }

    createShipmentReport(){
        return axios.post(`${API_URL}/main/report/shipment/`);
    }

    findShipmentsBySenderOrReceiver(filter){
        return axios.get(`${API_URL}/main/shipment/search`, {params: {filter: filter}});
    }

    orderStoresByNameAsc(a, b){
        const locationA = a.name.toUpperCase();
        const locationB = b.name.toUpperCase();
        if (locationA < locationB){
            return -1;
        }
        if (locationA > locationB){
            return 1;
        }
        return 0;
    }
}

export default new ShipmentDataService()