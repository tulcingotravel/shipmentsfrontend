/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import { Component } from 'react';
import AuthenticationService from '../AuthenticationService.js';
import { NavLink as NavLinkRRD, Link } from 'react-router-dom';
// nodejs library to set properties for components
import { PropTypes } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// reactstrap components
import { Collapse,DropdownMenu,DropdownItem,UncontrolledDropdown,DropdownToggle,Form,Input,
  InputGroupAddon,InputGroupText,InputGroup,Media,NavbarBrand,Navbar,NavItem,NavLink,Nav,Container,
  Row,Col} from 'reactstrap';
import { withRouter } from 'react-router';
import brandImg from '../../assets/img/brand/tulcingo-travel.png';

class Sidebar extends Component {
  state = {
    collapseOpen: false
  };
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
  }
  // toggles collapse between opened and closed (true/false)
  toggleCollapse = () => {
    this.setState({
      collapseOpen: !this.state.collapseOpen
    });
  };
  // closes the collapse
  closeCollapse = () => {
    this.setState({
      collapseOpen: false
    });
  };
  // creates the links that appear in the left menu / Sidebar
  createLinks = (routes) => {
    return routes.map((prop, key) => {
      return (
        <NavItem key={key}>
          <NavLink
            to={prop.layout + prop.path}
            tag={NavLinkRRD}
            onClick={this.closeCollapse}
            activeClassName='active'
          >
            <i className={prop.icon} />
            {prop.name}
          </NavLink>
        </NavItem>
      );
    });
  };
  render() {
    const { bgColor, routes, logo } = this.props;
    const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
    const userLoggedIn = AuthenticationService.getLoggedInUserName();
    return (
      <Navbar
        className='navbar-vertical fixed-left navbar-light bg-white'
        expand='md'
        id='sidenav-main'
      >
        <Container fluid>
          {/* Toggler */}
          <button
            className='navbar-toggler'
            type='button'
            onClick={this.toggleCollapse}
          >
            <span className='navbar-toggler-icon' />
          </button>
          {/* Brand */}
          <NavbarBrand className='pt-0'>
            <div>
              <a href='/' className='navbar-brand'>
                Tulcingo travel
              </a>
            </div>
          </NavbarBrand>
          {/* User */}
          {isUserLoggedIn && (
            <Nav className='align-items-center d-md-none'>
              <UncontrolledDropdown nav>
                <DropdownToggle nav>
                  <Media className='align-items-center'>
                    <span className='avatar avatar-sm rounded-circle'>
                      <img
                        alt='Mi perfil'
                        src={brandImg}
                      />
                    </span>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-arrow' right>
                  <DropdownItem className='noti-title' header tag='div'>
                    <h6 className='text-overflow m-0'>
                      Bienvenido {userLoggedIn}!
                    </h6>
                  </DropdownItem>
                  <DropdownItem to='/admin/user-profile' tag={Link}>
                    <FontAwesomeIcon icon='user' />
                    <span>Mi perfil</span>
                  </DropdownItem>
                  <DropdownItem to='/admin/user-profile' tag={Link}>
                    <FontAwesomeIcon icon='tools' />
                    <span>Configuraciones</span>
                  </DropdownItem>
                  <DropdownItem to='/admin/user-profile' tag={Link}>
                    <FontAwesomeIcon icon='bell' />
                    <span>Alertas</span>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    <li>
                      <Link
                        to='/logout'
                        className='nav-link'
                        onClick={AuthenticationService.logout}
                      >
                        <FontAwesomeIcon icon='power-off' />
                        Salir
                      </Link>
                    </li>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          )}
          {/* Collapse */}
          <Collapse navbar isOpen={this.state.collapseOpen}>
            {/* Collapse header */}
            <div className='navbar-collapse-header d-md-none'>
              <Row>
                <Col className='collapse-close' xs='6'>
                  <button
                    className='navbar-toggler'
                    type='button'
                    onClick={this.toggleCollapse}
                  >
                    <span />
                    <span />
                  </button>
                </Col>
              </Row>
            </div>
            {/* Form */}
            <Form className='mt-4 mb-3 d-md-none'>
              <InputGroup className='input-group-rounded input-group-merge'>
                <Input
                  aria-label='Search'
                  className='form-control-rounded form-control-prepended'
                  placeholder='Buscar'
                  type='Search'
                />
                <InputGroupAddon addonType='prepend'>
                  <InputGroupText>
                    <FontAwesomeIcon icon='search' />
                  </InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </Form>
            {/* Navigation */}
            <h6 className='navbar-heading text-muted'>Envíos</h6>
            <Nav className='mb-md-3' navbar>
              <NavItem>
                {isUserLoggedIn && (
                  <li>
                    <Link to='/shipments' className='nav-link'>
                      <FontAwesomeIcon icon='boxes' />
                      Envíos
                    </Link>
                  </li>
                )}
              </NavItem>
              <NavItem>
                {isUserLoggedIn && (
                  <NavLink href=''>
                    <FontAwesomeIcon icon='box-open' />
                    Nuevo envío
                  </NavLink>
                )}
              </NavItem>
            </Nav>
            {/* Divider */}
            <hr className='my-3' />
            {/* Heading */}
            <h6 className='navbar-heading text-muted'>Personas</h6>
            {/* Navigation */}
            <Nav className='mb-md-3' navbar>
              <NavItem>
                {isUserLoggedIn && (
                  <li>
                    <Link to='/persons' className='nav-link'>
                      <FontAwesomeIcon icon='users' />
                      Ver todos
                    </Link>
                  </li>
                )}
              </NavItem>
              <NavItem>
                {isUserLoggedIn && (
                  <NavLink href=''>
                    <FontAwesomeIcon icon='user-plus' />
                    Nueva persona
                  </NavLink>
                )}
              </NavItem>
              <hr className='my-3' />
              <NavItem>
                {isUserLoggedIn && (
                  <li>
                    <Link
                      to='/logout'
                      className='nav-link'
                      onClick={AuthenticationService.logout}
                    >
                      <FontAwesomeIcon icon='power-off' />
                      Salir
                    </Link>
                  </li>
                )}
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

Sidebar.defaultProps = {
  routes: [{}]
};

Sidebar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired
  })
};

export default withRouter(Sidebar);
