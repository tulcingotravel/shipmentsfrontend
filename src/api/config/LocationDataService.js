import axios from "axios";
import { API_URL } from "../../Constants";

class LocationDataService{

    findAllOriginLocation(){
        return axios.get(`${API_URL}/config/location/origin`);
    }

    findAllDestinationLocation(){
        return axios.get(`${API_URL}/config/location/destination`);
    }

    findAllOriginLocations(){
        return axios.get(`${API_URL}/config/location/origins`);
    }
}

export default new LocationDataService()