/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink as NavLinkRRD, Link } from 'react-router-dom';
// nodejs library to set properties for components
import { PropTypes } from 'prop-types';

// reactstrap components
import {Collapse,DropdownMenu,DropdownItem,UncontrolledDropdown,DropdownToggle,Form,Input,InputGroupAddon,
  InputGroupText,InputGroup,Media,NavbarBrand,Navbar,NavItem,NavLink,Nav,Container,Row,Col} from 'reactstrap';
import brandImg from '../../assets/img/brand/tulcingo-travel.png';
//FOR MOBILE
class Sidebar extends React.Component {
  state = {
    collapseOpen: false,
    roleUser: localStorage.getItem('role')
  };
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
    this.setState({
      roleUser: localStorage.getItem('role')
    });
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
  }
  // toggles collapse between opened and closed (true/false)
  toggleCollapse = () => {
    this.setState({
      collapseOpen: !this.state.collapseOpen
    });
  };
  // closes the collapse
  closeCollapse = () => {
    this.setState({
      collapseOpen: false
    });
  };
  componentDidMount() {
    this.setState({
      roleUser: localStorage.getItem('role')
    });
  }

  // creates the links that appear in the left menu / Sidebar
  createLinks = (routes) => {
    return routes
      .filter((prop, key) => {
        return prop.role.find((role) => role == this.state.roleUser);
      })
      .map((prop, key) => {
        return (
          <NavItem key={key}>
            <NavLink
              to={prop.layout + prop.path}
              tag={NavLinkRRD}
              onClick={this.closeCollapse}
              activeClassName={this.activeRoute(prop.layout + prop.path)}
            >
              <FontAwesomeIcon
                icon={prop.icon}
                className={prop.className}
                size='2x'
              />
              <span>{prop.name}</span>
            </NavLink>
          </NavItem>
        );
      });
  };
  render() {
    const { routes, logo } = this.props;
    let navbarBrandProps;
    if (logo && logo.innerLink) {
      navbarBrandProps = {
        to: logo.innerLink,
        tag: Link
      };
    } else if (logo && logo.outterLink) {
      navbarBrandProps = {
        href: logo.outterLink,
        target: '_blank'
      };
    }
    return (
      <Navbar
        className='navbar-vertical fixed-left navbar-light bg-white'
        expand='md'
        id='sidenav-main'
      >
        <Container fluid>
          {/* Toggler */}
          <button
            className='navbar-toggler'
            type='button'
            onClick={this.toggleCollapse}
          >
            <span className='navbar-toggler-icon' />
          </button>
          {/* Brand */}
          <NavbarBrand className='pt-0' {...navbarBrandProps}>
            <img src={brandImg} />
          </NavbarBrand>
          {/* User */}
          <Nav className='align-items-center d-md-none'>
            <UncontrolledDropdown nav>
              <DropdownToggle nav>
                <Media className='align-items-center'>
                  <span className='avatar avatar-sm rounded-circle'>
                    <FontAwesomeIcon icon='user' className='text-primary' />
                  </span>
                </Media>
              </DropdownToggle>
              <DropdownMenu className='dropdown-menu-arrow' right>
                <DropdownItem className='noti-title' header tag='div'>
                  <h6 className='text-overflow m-0'>Bienvenido!</h6>
                </DropdownItem>
                <DropdownItem to='/admin/user-profile' tag={Link}>
                  <FontAwesomeIcon icon='user' />
                  <span>Mi perfil</span>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem to='/logout' onClick={this.logout}>
                  <FontAwesomeIcon icon='power-off' />
                  Salir
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          {/* Collapse */}
          <Collapse navbar isOpen={this.state.collapseOpen}>
            {/* Collapse header */}
            <div className='navbar-collapse-header d-md-none'>
              <Row>
                {logo ? (
                  <Col className='collapse-brand' xs='6'>
                    {logo.innerLink ? (
                      <Link to={logo.innerLink}>
                        <img alt={logo.imgAlt} src={brandImg} />
                      </Link>
                    ) : (
                      <a href={logo.outterLink}>
                        <img alt={logo.imgAlt} src={brandImg} />
                      </a>
                    )}
                  </Col>
                ) : null}
                <Col className='collapse-close' xs='6'>
                  <button
                    className='navbar-toggler'
                    type='button'
                    onClick={this.toggleCollapse}
                  >
                    <span />
                    <span />
                  </button>
                </Col>
              </Row>
            </div>
            {/* Form */}
            <Form className='mt-4 mb-3 d-md-none'>
              <InputGroup className='input-group-rounded input-group-merge'>
                <InputGroupAddon addonType='prepend'>
                  <InputGroupText>
                    <FontAwesomeIcon icon='search' />
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  aria-label='Search'
                  className='form-control-rounded form-control-prepended'
                  placeholder='Buscar...'
                  type='search'
                />
              </InputGroup>
            </Form>
            {/* Navigation */}
            <Nav navbar>{this.createLinks(routes)}</Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

Sidebar.defaultProps = {
  routes: [{}]
};

Sidebar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired
  })
};

export default Sidebar;
