import axios from "axios";
import { API_URL } from "../../Constants";

class PersonDataService{

    findAll(){
        return axios.get(`${API_URL}/config/person/`);
    }

    findPersonById(id){
        return axios.get(`${API_URL}/config/person/${id}`);
    }
    
    updatePerson(id, person){
        return axios.put(`${API_URL}/config/person/${id}`, person);
    }

    createPerson(person){
        return axios.post(`${API_URL}/config/person/`, person);
    }

    deletePerson(id){
        return axios.delete(`${API_URL}/config/person/${id}`);
    }
}

export default new PersonDataService()